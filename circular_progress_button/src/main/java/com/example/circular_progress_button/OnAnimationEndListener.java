package com.example.circular_progress_button;

interface OnAnimationEndListener {

    void onAnimationEnd();
}
