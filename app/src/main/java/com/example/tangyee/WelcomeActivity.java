package com.example.tangyee;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.tangyee.utils.SessionidUtils;

public class WelcomeActivity extends AppCompatActivity {

    private static final int TIME = 2000;
    private static final int GO_LOGIN = 0;
    private static final int GO_HOME = 1;

    private Handler mHandler = new Handler(msg -> {
        switch (msg.what) {
            case GO_LOGIN:
                goLogin();
                return true;
            case GO_HOME:
                goHome();
                return true;
        }
        return false;
    });

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        init();
    }

    private void init() {
        String sessionid = SessionidUtils.getSessionId(this);
        if (sessionid == null) {
            mHandler.sendEmptyMessageDelayed(GO_LOGIN, TIME);
        } else {
            mHandler.sendEmptyMessageDelayed(GO_HOME, TIME);
        }
    }

    private void goHome() {
        Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
        startActivity(intent);
        // 同时把不用的界面销毁掉
        finish();
    }

    private void goLogin() {
        Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
