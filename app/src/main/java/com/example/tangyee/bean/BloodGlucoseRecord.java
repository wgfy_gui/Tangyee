package com.example.tangyee.bean;

import java.util.Date;

public class BloodGlucoseRecord {
    private int id;
    private int userId;
    private Date recordDate;
    private double mbg;
    private double bbfbg;
    private double abfbg;
    private double blbg;
    private double albg;
    private double bdbg;
    private double adbg;
    private double btbg;

    int low, good, high;

    public BloodGlucoseRecord() { }

    public BloodGlucoseRecord(int id, int userId, Date recordDate, double mbg, double bbfbg, double abfbg, double blbg, double albg, double bdbg, double adbg, double btbg) {
        this.id = id;
        this.userId = userId;
        this.recordDate = recordDate;
        this.mbg = mbg;
        this.bbfbg = bbfbg;
        this.abfbg = abfbg;
        this.blbg = blbg;
        this.albg = albg;
        this.bdbg = bdbg;
        this.adbg = adbg;
        this.btbg = btbg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public double getMbg() {
        return mbg;
    }

    public void setMbg(double mbg) {
        this.mbg = mbg;
    }

    public double getBbfbg() {
        return bbfbg;
    }

    public void setBbfbg(double bbfbg) {
        this.bbfbg = bbfbg;
    }

    public double getAbfbg() {
        return abfbg;
    }

    public void setAbfbg(double abfbg) {
        this.abfbg = abfbg;
    }

    public double getBlbg() {
        return blbg;
    }

    public void setBlbg(double blbg) {
        this.blbg = blbg;
    }

    public double getAlbg() {
        return albg;
    }

    public void setAlbg(double albg) {
        this.albg = albg;
    }

    public double getBdbg() {
        return bdbg;
    }

    public void setBdbg(double bdbg) {
        this.bdbg = bdbg;
    }

    public double getAdbg() {
        return adbg;
    }

    public void setAdbg(double adbg) {
        this.adbg = adbg;
    }

    public double getBtbg() {
        return btbg;
    }

    public void setBtbg(double btbg) {
        this.btbg = btbg;
    }

    public double getDataBySign(int sign) {
        switch (sign) {
            case 1:
                return mbg;
            case 2:
                return bbfbg;
            case 3:
                return abfbg;
            case 4:
                return blbg;
            case 5:
                return albg;
            case 6:
                return bdbg;
            case 7:
                return adbg;
            case 8:
                return btbg;
        }
        return 0;
    }

    public void setDataBySign(int sign, double d) {
        switch (sign) {
            case 1:
                this.mbg = d;
                break;
            case 2:
                this.bbfbg = d;
                break;
            case 3:
                this.abfbg = d;
                break;
            case 4:
                this.blbg = d;
                break;
            case 5:
                this.albg = d;
                break;
            case 6:
                this.bdbg = d;
                break;
            case 7:
                this.adbg = d;
                break;
            case 8:
                this.btbg = d;
                break;
        }
    }

    @Override
    public String toString() {
        return "BloodGlucoseRecord{" +
                "id=" + id +
                ", userId=" + userId +
                ", recordDate=" + recordDate +
                ", mbg=" + mbg +
                ", bbfbg=" + bbfbg +
                ", abfbg=" + abfbg +
                ", blbg=" + blbg +
                ", albg=" + albg +
                ", bdbg=" + bdbg +
                ", adbg=" + adbg +
                ", btbg=" + btbg +
                '}';
    }
}
