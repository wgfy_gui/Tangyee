package com.example.tangyee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class Exercise implements Parcelable {

    private String dateStr;
    private String name;
    private String time;
    private String calorie;

    public Exercise(){}

    protected Exercise(Parcel in) {
        dateStr = in.readString();
        name = in.readString();
        time = in.readString();
        calorie = in.readString();
    }

    public static final Creator<Exercise> CREATOR = new Creator<Exercise>() {
        @Override
        public Exercise createFromParcel(Parcel in) {
            return new Exercise(in);
        }

        @Override
        public Exercise[] newArray(int size) {
            return new Exercise[size];
        }
    };

    public String getDateStr() {
        return dateStr;
    }

    public void setDateStr(String dateStr) {
        this.dateStr = dateStr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCalorie() {
        return calorie;
    }

    public void setCalorie(String calorie) {
        this.calorie = calorie;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "dateStr='" + dateStr + '\'' +
                ", name='" + name + '\'' +
                ", time='" + time + '\'' +
                ", calorie='" + calorie + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dateStr);
        dest.writeString(name);
        dest.writeString(time);
        dest.writeString(calorie);
    }
}
