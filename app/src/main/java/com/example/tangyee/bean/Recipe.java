package com.example.tangyee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class Recipe implements Parcelable {

    private int id;
    private String recipeName;
    private String main;
    private String seasoning;
    private String method;
    private String tips;
    private int imgId = -1;
    private int pepperLevel;

    private String image;
    private int readingNum;

    public Recipe() { }

    protected Recipe(Parcel in) {
        id = in.readInt();
        recipeName = in.readString();
        main = in.readString();
        seasoning = in.readString();
        method = in.readString();
        tips = in.readString();
        imgId = in.readInt();
        pepperLevel = in.readInt();
        image = in.readString();
        readingNum = in.readInt();
    }

    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getMain() {
        return main != null ? main : "";
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getMethod() {
        return method != null ? method : "";
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getTips() {
        return tips;
    }

    public void setTips(String tips) {
        this.tips = tips;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

    public int getPepperLevel() {
        return pepperLevel;
    }

    public String getSeasoning() {
        return seasoning != null ? seasoning : "";
    }

    public void setSeasoning(String seasoning) {
        this.seasoning = seasoning;
        if (seasoning == "null") {
            this.seasoning = "";
        }
    }

    public int getReadingNum() {
        return readingNum;
    }

    public void setReadingNum(int readingNum) {
        this.readingNum = readingNum;
    }

    public void setPepperLevel(int pepperLevel) {
        this.pepperLevel = pepperLevel;
    }

    public String getImageUrl() {
        return "https://www.tangyee.com/Content/Upload/Images/Recipe/" + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Recipe{" +
                "id='" + id + '\'' +
                ", recipeName='" + recipeName + '\'' +
                ", main='" + main + '\'' +
                ", method='" + method + '\'' +
                ", imgId=" + imgId +
                ", pepperLevel=" + pepperLevel +
                ", image='" + image + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(recipeName);
        dest.writeString(main);
        dest.writeString(seasoning);
        dest.writeString(method);
        dest.writeString(tips);
        dest.writeInt(imgId);
        dest.writeInt(pepperLevel);
        dest.writeString(image);
        dest.writeInt(readingNum);
    }
}
