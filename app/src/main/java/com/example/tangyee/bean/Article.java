package com.example.tangyee.bean;

public class Article {
    private int knowID;
    private String title;
    private String author = "糖家";
    private int count;
    private String releaseDate;
    private String summary;
    private String image;
    private String content;

    public Article() { }

    public Article(int knowID, String title, String author, int count, String releaseDate, String summary, String image) {
        this.knowID = knowID;
        this.title = title;
        this.author = author;
        this.count = count;
        this.releaseDate = releaseDate;
        this.summary = summary;
        this.image = image;
    }

    public int getKnowID() {
        return knowID;
    }

    public void setKnowID(int knowID) {
        this.knowID = knowID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentUrl() {
        return "https://www.tangyee.com/Mobile/Knowledge/GetKnowledgeInfo?kid=" + knowID;
    }

    public String getImageUrl() {
        return "https://www.tangyee.com/Content/Upload/Images/Knowledge/" + image;
    }
}
