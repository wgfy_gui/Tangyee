package com.example.tangyee.bean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

public class Question implements Parcelable {

    private String questionId;
    private String questionTitle;
    private Map<String, Integer> answerItems;
    private int score = -1;

    public Question() {}

    protected Question(Parcel in) {
        questionId = in.readString();
        questionTitle = in.readString();
        score = in.readInt();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionTitle() {
        return questionTitle;
    }

    public void setQuestionTitle(String questionTitle) {
        this.questionTitle = questionTitle;
    }

    public Map<String, Integer> getAnswerItems() {
        return answerItems;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setAnswerItems(Map<String, Integer> answerItems) {
        this.answerItems = answerItems;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(questionId);
        dest.writeString(questionTitle);
        dest.writeInt(score);
    }
}
