package com.example.tangyee.bean;

/**
 * 每一条知识的信息
 * @author: yu huawen
 * @date: 2019/1/24 10:00
 */
public class KnowledgeItem {
    // 作者
    private String author;
    // 访问人数
    private int count;
    // 图片(糖尿病.png)
    private String image;
    // 知识id
    private int knowID;
    // 发布日期
    private String releaseDate;
    // 摘要
    private String summary;
    // 标题
    private String title;
    // 详细内容
    private String content;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getKnowID() {
        return knowID;
    }

    public void setKnowID(int knowID) {
        this.knowID = knowID;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String toString(){
        return getAuthor() + getCount() + getImage() + getKnowID() + getReleaseDate() + getSummary() +getTitle();
    }
}
