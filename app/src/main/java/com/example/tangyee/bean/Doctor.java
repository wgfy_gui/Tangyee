package com.example.tangyee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class Doctor implements Parcelable {

    private String homeUrl = "https://www.tangyee.com";

    private String detailUrl;
    private String imgUrl;
    private String name;
    private String describe;

    private String major;
    private String jobTitle;
    private String detail;

    public Doctor(){};

    protected Doctor(Parcel in) {
        homeUrl = in.readString();
        detailUrl = in.readString();
        imgUrl = in.readString();
        name = in.readString();
        describe = in.readString();
    }

    public static final Creator<Doctor> CREATOR = new Creator<Doctor>() {
        @Override
        public Doctor createFromParcel(Parcel in) {
            return new Doctor(in);
        }

        @Override
        public Doctor[] newArray(int size) {
            return new Doctor[size];
        }
    };

    public String getDetailUrl() {
        return homeUrl + detailUrl;
    }

    public void setDetailUrl(String detailUrl) {
        this.detailUrl = detailUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = homeUrl + imgUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "homeUrl='" + homeUrl + '\'' +
                ", detailUrl='" + detailUrl + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", name='" + name + '\'' +
                ", describe='" + describe + '\'' +
                ", major='" + major + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(homeUrl);
        dest.writeString(detailUrl);
        dest.writeString(imgUrl);
        dest.writeString(name);
        dest.writeString(describe);
    }
}
