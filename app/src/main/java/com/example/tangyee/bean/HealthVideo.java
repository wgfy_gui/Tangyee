package com.example.tangyee.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class HealthVideo implements Parcelable {

    private String videoId;
    private String image;
    private String title;
    private String type;
    private String heat;

    private String url;
    private String desc;
    private String promulgator;
    private String time;

    public HealthVideo() { }

    protected HealthVideo(Parcel in) {
        videoId = in.readString();
        image = in.readString();
        title = in.readString();
        type = in.readString();
        heat = in.readString();
        url = in.readString();
        desc = in.readString();
        promulgator = in.readString();
        time = in.readString();
    }

    public static final Creator<HealthVideo> CREATOR = new Creator<HealthVideo>() {
        @Override
        public HealthVideo createFromParcel(Parcel in) {
            return new HealthVideo(in);
        }

        @Override
        public HealthVideo[] newArray(int size) {
            return new HealthVideo[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPromulgator() {
        return promulgator;
    }

    public void setPromulgator(String promulgator) {
        this.promulgator = promulgator;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getHeat() {
        return heat;
    }

    public void setHeat(String heat) {
        this.heat = heat;
    }

    public String getImageUrl() {
        return "https://www.tangyee.com" + image;
    }

    public String getVideoPageUrl() {
        return "https://www.tangyee.com/Mui/Page/" + videoId;
    }

    @Override
    public String toString() {
        return "HealthVideo{" +
                "videoId='" + videoId + '\'' +
                ", image='" + image + '\'' +
                ", title='" + title + '\'' +
                ", type='" + type + '\'' +
                ", heat='" + heat + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(videoId);
        dest.writeString(image);
        dest.writeString(title);
        dest.writeString(type);
        dest.writeString(heat);
        dest.writeString(url);
        dest.writeString(desc);
        dest.writeString(promulgator);
        dest.writeString(time);
    }
}
