package com.example.tangyee.bean;

public class KnowledgeMenuItem {

    private int id;
    private String category;
    private int parentId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "KnowledgeMenuItem{" +
                "id=" + id +
                ", category='" + category + '\'' +
                ", parentId=" + parentId +
                '}';
    }
}
