package com.example.tangyee.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.example.circular_progress_button.CircularProgressButton;
import com.example.tangyee.MainActivity;
import com.example.tangyee.R;
import com.example.tangyee.utils.ConstantsUtil;
import com.example.tangyee.utils.SessionidUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class RegisterFragment extends Fragment {
    private static final String  TAG = "RegisterFragment";

    private Unbinder mUnbinder;

    // 跟踪登录（后台）任务，确保我们可以在需要时取消它。
    private UserRegisterTask mAuthTask = null;

    @BindView(R.id.phone_num)
    AutoCompleteTextView mPhoneNumView;
    @BindView(R.id.username)
    EditText mUsernameView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.sign_in_button)
    CircularProgressButton mSignInButton;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_register, container, false);
        ButterKnife.bind(this, v);

        mPasswordView.setOnEditorActionListener((textView, id, keyEvent) -> {
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptRegister();
                return true;
            }
            return false;
        });

        mSignInButton.setText("注册");

        mSignInButton.setIndeterminateProgressMode(true); // 进入不精准进度模式
        mSignInButton.setOnClickListener(v1 -> attemptRegister());

        return v;
    }


    /**
     * 尝试注册
     */
    private void attemptRegister() {
        if (mAuthTask != null) {
            return;
        }

        // 关闭输入法
        InputMethodManager inputMethodManager = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);

        //接受软键盘输入的编辑文本或其它视图
//        inputMethodManager.showSoftInput(submitBt,InputMethodManager.SHOW_FORCED);


        // 重置错误信息
        mPhoneNumView.setError(null);
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // 保存/提取这次尝试登陆的账号和密码
        String phoneNum = mPhoneNumView.getText().toString();
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        // 是否取消登陆
        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(phoneNum)) {
            mPhoneNumView.setError(getString(R.string.error_field_required));
            focusView = mPhoneNumView;
            cancel = true;
        } else if (!isPhoneNumValid(phoneNum)) {
            mPhoneNumView.setError("手机号长度有误（11位）");
            focusView = mPhoneNumView;
            cancel = true;
        }


        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            mUsernameView.setError("账号长度应在5-10位之间");
            focusView = mUsernameView;
            cancel = true;
        }

        // 如果用户输入了密码，检查它是否有效
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // 登陆表单有错，不再试图登陆，先提示错误
            // 表单字段有误 则显示错误信息
            focusView.requestFocus();
        } else {
            // 显示一个进度圈，同时开始一个后台任务
            // 执行用户登陆的意图
            showProgress(true);

            mAuthTask = new UserRegisterTask(phoneNum, username, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isPhoneNumValid(String phoneNum) {
        return phoneNum.length() == 11;
    }
    private boolean isUsernameValid(String username) {
        return username.length() >= 5 && username.length() <= 10;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5;
    }

    /**
     * 展示进度UI并隐藏登陆表格
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {

        if (show) {
            // 按钮不可点击
            mSignInButton.setClickable(false);
            // 显示进度条
            mSignInButton.setProgress(50);
        } else {
            // 还原按钮
            mSignInButton.setProgress(0);
            mSignInButton.setClickable(true);
        }
    }

    /**
     * 表示 用户（账密格式无误的） 的一个异步 登陆/注册 任务
     */
    public class UserRegisterTask extends AsyncTask<Void, Void, String> {

        private final String mUsername;
        private final String mPassword;
        private final String mPhoneNum;

        UserRegisterTask(String phoneNum, String username, String password) {
            mPhoneNum = phoneNum;
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected String doInBackground(Void... params) {

            OkHttpClient okHttpClient = new OkHttpClient();

            RequestBody body = new FormBody.Builder()
                    .add("username", mUsername)
                    .add("pwd", mPassword)
                    .add("phone", mPhoneNum)
                    .add("usertype", "2")
                    .build();

            Request request = new Request.Builder()
                    .url(ConstantsUtil.REGISTER)
                    .post(body)
                    .build();

            try {
                Response response = okHttpClient.newCall(request).execute();
                if (response.isSuccessful()) {
                    Log.d(TAG, "doInBackground: " + response.body().toString());
                    return response.body().string();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(final String response) {
            mAuthTask = null;

            if (response == null) {
                showProgress(false);
                Snackbar.make(getView(), "注册失败，出现异常。", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                return;
            }

            try {
                Log.i(TAG, "Received JSON: " + response);
                JSONObject jsonBody = new JSONObject(response);
                if (jsonBody.getInt("state") == 4) {
                    SessionidUtils.setSessionId(getActivity(), jsonBody.getString("sessionid"), mUsername, mPassword);
                    loginSuccess();
                } else if (jsonBody.getInt("state") == 1) {
                    showProgress(false);
                    mPasswordView.setError("该用户已经注册");
                    mPasswordView.requestFocus();
                } else {
                    showProgress(false);
                    mPasswordView.setError(getString(R.string.error_register));
                    mPasswordView.requestFocus();
                }
            } catch (JSONException je) {
                Log.e(TAG, "Failed to parse JSON", je);
                Snackbar.make(getView(), "注册失败，服务器繁忙。", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                showProgress(false);
            }

        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private void loginSuccess() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
