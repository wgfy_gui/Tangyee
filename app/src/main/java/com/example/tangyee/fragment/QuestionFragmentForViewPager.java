package com.example.tangyee.fragment;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.tangyee.R;
import com.example.tangyee.activity.PredictActivity;
import com.example.tangyee.bean.Question;
import com.example.tangyee.utils.DensityUtils;

import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class QuestionFragmentForViewPager extends Fragment {

    private static final String TAG = "QuestionFragment";

    private static final String PARAM = "index";

    private Unbinder mUnbinder;

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.radioGroup)
    RadioGroup mRadioGroup;

    private Question mQuestion;
    private int index;

    private Handler mHandler = new Handler(msg -> {
        switch (msg.what) {
            case 1:
                if (getActivity() == null) {
                    return false;
                }
                ((PredictActivity) getActivity()).selectNextTab();
                return true;
        }
        return false;
    });

    public static QuestionFragmentForViewPager newInstance(Context context, int index) {
        Bundle bundle = new Bundle();
        bundle.putInt(PARAM, index);
        QuestionFragmentForViewPager questionFragmentForViewPager = new QuestionFragmentForViewPager();
        questionFragmentForViewPager.setArguments(bundle);
        return questionFragmentForViewPager;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        index = getArguments().getInt(PARAM);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question, container, false);
        mUnbinder = ButterKnife.bind(this,view);
        mQuestion = ((PredictActivity) getActivity()).data.get(index);

        initView();

        return view;
    }

    private void initView() {
        title.setText(mQuestion.getQuestionTitle());
        for (Map.Entry<String, Integer> entry : mQuestion.getAnswerItems().entrySet()) {
            RadioButton radioButton = new RadioButton(getActivity());
            radioButton.setText(entry.getKey());
            radioButton.setTag(entry.getValue());
            radioButton.setTextSize(18);
            radioButton.setButtonDrawable(null);
            radioButton.setBackground(getResources().getDrawable(R.drawable.radiobutton_background));
            radioButton.setTextColor(getResources().getColor(R.color.radiobutton_textcolor));
            radioButton.setPadding(DensityUtils.dp2px(getActivity(), 10), DensityUtils.dp2px(getActivity(), 10), DensityUtils.dp2px(getActivity(), 10), DensityUtils.dp2px(getActivity(), 10));
            radioButton.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

//            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(DensityUtils.dp2px(getActivity(), 10), DensityUtils.dp2px(getActivity(), 10), DensityUtils.dp2px(getActivity(), 10), DensityUtils.dp2px(getActivity(), 10));

            mRadioGroup.addView(radioButton, params);
        }
        mRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            try {
                RadioButton tempButton = getActivity().findViewById(checkedId);
                mQuestion.setScore(Objects.requireNonNull(mQuestion.getAnswerItems().get(tempButton.getText())));
            } catch (NullPointerException e) {
                Log.e(TAG, "onCheckedChanged: ", e);
            }
            mHandler.postDelayed(() -> {
                Message msg = Message.obtain();
                msg.what = 1;
                mHandler.sendMessage(msg);
            }, 200);
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
