package com.example.tangyee.utils;

import android.content.Context;
import android.preference.PreferenceManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SessionidUtils {

    private static final String SESSION_ID = "sessionid";

    private static final String USERNAME = "username";
    private static final String PASSWORD = "password";

    public static String getSessionId(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(SESSION_ID, null);
    }

    public static String getCookie(Context context) {
        String sessionid = PreferenceManager.getDefaultSharedPreferences(context).getString(SESSION_ID, null);
        if (sessionid == null) {
            return null;
        }
        return "ASP.NET_SessionId=" + sessionid;
    }

    public static void setSessionId(Context context, String sessionid, String username, String password) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(SESSION_ID, sessionid)
                .putString(USERNAME, username)
                .putString(PASSWORD, password)
                .apply();
    }

    public static void setSessionId(Context context, String sessionid) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(SESSION_ID, sessionid)
                .apply();
    }

    public static void clear(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .clear()
                .apply();
    }

    public static void updateSessionId(Context context) {
        String username = PreferenceManager.getDefaultSharedPreferences(context).getString(USERNAME, null);
        String password = PreferenceManager.getDefaultSharedPreferences(context).getString(PASSWORD, null);

        OkHttpClient okHttpClient = new OkHttpClient();

        RequestBody body = new FormBody.Builder()
                .add("user", username)
                .add("pwd", password)
                .build();

        Request request = new Request.Builder()
                .url(ConstantsUtil.LOGIN)
                .post(body)
                .build();

        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response.isSuccessful()) {
                String responseStr = response.body().string();
                JSONObject jsonObject = new JSONObject(responseStr);
                setSessionId(context, jsonObject.getString("sessionid"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, "本地账户信息异常，请清除数据重新登陆。",Toast.LENGTH_SHORT).show();
        }

    }

}
