package com.example.tangyee.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.example.tangyee.bean.Article;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetArticles extends AsyncTask<Void, Void, List<Article>> {

    private static final String TAG = "GetArticles";

    private int page;
    private int pageSize;
    private OnPostExecute mExecute;

    /*不应持有Context，会引起内存泄漏*/
    // private Context mContext;

    public GetArticles(int page, int pageSize, OnPostExecute execute){
        this.page = page;
        this.pageSize = pageSize;
        this.mExecute = execute;
    }

    @Override
    protected List<Article> doInBackground(Void... voids) {
        if (isCancelled()){
            return new ArrayList<>();
        }
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .retryOnConnectionFailure(false)
                .build();

        StringBuilder stringBuilder = new StringBuilder(ConstantsUtil.GET_ARTICLES);
        stringBuilder.append("page=" + page);
        stringBuilder.append("&pagesize=" + pageSize);
        Request request = new Request.Builder()
                .url(stringBuilder.toString())
                .build();

        String respStr = null;
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (response == null) {
                return null;
            }
            respStr = response.body().string();

            if (respStr == null) {
                return null;
            }
        } catch (IOException e) {
            Log.e(TAG, "doInBackground: ", e);
        }
        List<Article> items = new ArrayList<>();

        try {
            Log.i(TAG, "Received JSON: " + respStr);
            if (respStr == null) {
                return null;
            }
            JSONArray jsonArray = new JSONArray(respStr);
            // 将json转成list
            parseItems(items, jsonArray);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }  catch (NullPointerException npe) {
            Log.e(TAG, "doInBackground: ", npe);
            return null;
        }

        return items;
    }

    @Override
    protected void onPostExecute(List<Article> articles) {
        mExecute.onPostExecute(articles);
    }

    private void parseItems(List<Article> items, JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i ++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            Article article = new Article();
            article.setKnowID(jsonObject.getInt("knowID"));
            article.setTitle(jsonObject.getString("title"));
            article.setAuthor(jsonObject.getString("author"));
            try {
                article.setCount(jsonObject.getInt("count"));
            } catch (JSONException e) {
                Log.w(TAG, "Failed to parse JSON to count", e);
                article.setCount(0);
            }
            article.setReleaseDate(jsonObject.getString("releaseDate"));
            article.setSummary(jsonObject.getString("summary"));
            article.setImage(jsonObject.getString("image"));

            items.add(article);
        }
    }

    public void setOnPostExecute(OnPostExecute execute) {
        this.mExecute = execute;
    }

    public interface OnPostExecute {
        void onPostExecute(List<Article> articles);
    }
}
