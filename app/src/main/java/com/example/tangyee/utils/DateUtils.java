package com.example.tangyee.utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtils {

    public static Date getPreDate(Date date, int pre) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        int day = calendar.get(Calendar.DATE);
        calendar.set(Calendar.DATE, day - pre);
        return calendar.getTime();
    }

    public static boolean isSameDay(Date d1, Date d2) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Log.d("DateUtils", "isSameDay: " + d1.toLocaleString() + "|" + d2.toLocaleString());
        return simpleDateFormat.format(d1).equals(simpleDateFormat.format(d2));
    }

    public static String getTimeDescription(int sign) {
        switch (sign-1){
            case 0: return "凌晨";
            case 1: return "早餐前";
            case 2: return "早餐后";
            case 3: return "午餐前";
            case 4: return "午餐后";
            case 5: return "晚餐前";
            case 6: return "晚餐后";
            case 7: return "睡前";
        }
        return "";
    }


    public static String getTimeDescriptionEn(int sign) {
        switch (sign-1){
            case 0: return "MBG";
            case 1: return "BBFBG";
            case 2: return "ABFBG";
            case 3: return "BLBG";
            case 4: return "ALBG";
            case 5: return "BDBG";
            case 6: return "ADBG";
            case 7: return "BTBG";
        }
        return "";
    }

}
