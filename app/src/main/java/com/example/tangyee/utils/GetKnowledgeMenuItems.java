package com.example.tangyee.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.example.tangyee.bean.Article;
import com.example.tangyee.bean.KnowledgeItem;
import com.example.tangyee.bean.KnowledgeMenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class GetKnowledgeMenuItems extends AsyncTask<Void, Void, List<KnowledgeMenuItem>> {

    private static final String TAG = "GetKnowledgeMenuItems";

    private int parentID;
    private OnPostExecute mExecute;

    public GetKnowledgeMenuItems(int parentID, OnPostExecute execute){
        this.parentID = parentID;
        this.mExecute = execute;
    }

    @Override
    protected List<KnowledgeMenuItem> doInBackground(Void... voids) {
        if (isCancelled()) {
            return new ArrayList<>();
        }
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .retryOnConnectionFailure(false)
                .build();

        StringBuilder stringBuilder = new StringBuilder(ConstantsUtil.GET_KNOWLEDGE_TYPE_ITEM);
        stringBuilder.append("parentID=" + parentID);
        Request request = new Request.Builder()
                .url(stringBuilder.toString())
                .build();

        String respStr = null;
        try {
            Response response = okHttpClient.newCall(request).execute();
            respStr = response.body().string();

            if (respStr == null) {
                return null;
            }
        } catch (IOException e) {
            Log.e(TAG, "doInBackground: ", e);
        }
        List<KnowledgeMenuItem> items = new ArrayList<>();

        try {
            Log.i(TAG, "Received JSON: " + respStr);
            JSONArray jsonArray = new JSONArray(respStr);
            // 将json转成list
            parseItems(items, jsonArray);
        } catch (JSONException je) {
            Log.e(TAG, "Failed to parse JSON", je);
        }  catch (NullPointerException npe) {
            Log.e(TAG, "doInBackground: ", npe);
            return null;
        }

        return items;
    }

    @Override
    protected void onPostExecute(List<KnowledgeMenuItem> items) {
        mExecute.onPostExecute(items);
    }

    private void parseItems(List<KnowledgeMenuItem> items, JSONArray jsonArray) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i ++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            KnowledgeMenuItem item = new KnowledgeMenuItem();

            item.setId(jsonObject.getInt("ID"));
            item.setParentId(jsonObject.getInt("ParentId"));
            item.setCategory(jsonObject.getString("Category"));

            items.add(item);
        }
    }

    public void setOnPostExecute(OnPostExecute execute) {
        this.mExecute = execute;
    }

    public interface OnPostExecute {
        void onPostExecute(List<KnowledgeMenuItem> items);
    }
}
