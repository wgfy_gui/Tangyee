package com.example.tangyee.utils;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Iterator;

/**
 * @author: yu huawen
 * @date: 2019/1/28 20:59
 */
public class JsoupFilterHTML {
    /**
     * 找出所有指定的标签，并在该标签的 Style 对属性的值进行修改
     * @param document
     * @param tag 要修改的标签
     * @param attr 要修改的属性
     * @param newValue 要修改的属性值
     */
    public static void jsoupFilterStyle(Document document, String tag, String attr, String newValue){
        Elements esd = document.select(tag);
        Iterator<Element> iterator = esd.iterator();
        while (iterator.hasNext()) {
            Element etemp = iterator.next();
            String styleStr = etemp.attr("style");
            etemp.removeAttr("style");
            etemp.attr("style", cssStr(styleStr, attr, newValue));
        }
    }

    /**
     *
     * @param styleStr 原先 Style 里面的内容
     * @param attr
     * @param newValue
     * @return
     */
    public static String cssStr(String styleStr, String attr, String newValue) {
        if (!styleStr.contains(attr)) {
            return styleStr;
        }
        String s1 = styleStr.substring(0, styleStr.indexOf(attr)); // attr 的前部分
        String s2 = styleStr.substring(styleStr.indexOf(attr), styleStr.length()); // attr 及其右边
        String s3 = s2.substring(s2.indexOf(";"));  // 逗号及其右边

        return s1 + attr + ":" + newValue + s3;
    }
}
