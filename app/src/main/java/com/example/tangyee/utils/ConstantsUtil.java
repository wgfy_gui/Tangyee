package com.example.tangyee.utils;

public class ConstantsUtil {

    // 获取运动项
    public static final String GET_EXERCISE_ITEMS = "https://www.tangyee.com/Mobile/Health/GetSportItems";
    // 保存运动记录
    public static final String SET_EXERCISE_RECORD = "https://www.tangyee.com/Mobile/Health/SetSport";
    // 获取运动记录
    public static final String GET_EXERCISES= "https://www.tangyee.com/Mui/Mgt/Sport";
    // 获取健康评估的问题
    public static final String GET_QUESTIONS= "https://www.tangyee.com/Assets/Plugin/question/diabetesQuestions.js";
    // 获取食谱详情
    public static final String GET_RECIPE_DETAIL = "https://www.tangyee.com/Mui/Page/RecipeDetail/";
    // 搜索
    public static final String SEARCH = "https://www.tangyee.com/Mui/Page/Search";
    // 获取 设置/管理 界面的信息
    public static final String SETTING = "https://www.tangyee.com/Mui/Mgt/Setting";
    // 获取套餐数据
    public static final String GET_MEAL_SETS = "https://www.tangyee.com/Pub/DataInJson/MealSetsInJson";
    // 获取血糖数据
    public static final String GET_BLOOD_GLUCOSE_DATA = "https://www.tangyee.com/Mobile/Health/GetBloodGlucose";
    // 设置血糖数据
    public static final String SET_BLOOD_GLUCOSE_DATA = "https://www.tangyee.com/Mobile/Health/SetBloodGlucose";
    // 获取知识文章
    public static final String GET_ARTICLES = "https://www.tangyee.com/Mobile/Knowledge/GetKnowledgeInfos?";
    // 通过文字类型获取文章
    public static final String GET_ARTICLES_FOR_TYPE = "https://www.tangyee.com/Mobile/Knowledge/GetKnowledgesByCls?";
    // 获取医生信息
    public static final String GET_DOCTORS = "https://www.tangyee.com/Mui/Page/DocList/";
    // 获取健康视频列表
    public static final String GET_HEALTH_VIDEOS = "https://www.tangyee.com/Mui/Page/VideoList/";
    // 获取知识分类的列表项
    public static final String GET_KNOWLEDGE_TYPE_ITEM = "https://www.tangyee.com/Mobile/Knowledge/GetKlgTopClses?";
    // 获取食谱成分
    public static final String GET_RECIPE_INGREDIENT = "https://www.tangyee.com/Pub/DataInJson/RecipeConstituentInJson?";
    // 获取食谱列表
    public static final String GET_RECIPES = "https://www.tangyee.com/Mobile/Recipe/GetRecipeInfosWithDefaultImg?";
    // 登陆
    public static final String LOGIN = "https://www.tangyee.com/Mobile/Login";
    // 注册
    public static final String REGISTER = "https://www.tangyee.com/Mobile/Login/Reg";
    // 维护信息
    public static final String UPDATE_INFO = "https://www.tangyee.com/Mobile/Login/UpdateUInfo";

}
