package com.example.tangyee;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import com.example.tangyee.activity.SearchActivity;
import com.example.tangyee.fragment.MealFragment;
import com.example.tangyee.fragment.HomeFragment;
import com.example.tangyee.fragment.KnowledgeFragment;
import com.example.tangyee.fragment.ManagerFragment;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationItem;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;
import com.luseen.luseenbottomnavigation.BottomNavigation.OnBottomNavigationItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private Unbinder mUnbinder;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.logo_img)
    ImageView logoImg;
    @BindView(R.id.search_img)
    ImageView searchImg;

    @BindView(R.id.app_bar_layout)
    AppBarLayout mAppBarLayout;

    private FragmentManager mFragmentManager;
    private Fragment currentFragment;
    private Fragment homeFragment, knowledgeFragment, mealFragment, managerFragment;

    private List<Fragment> mFragments = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //透明状态栏效果
        /*if (Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        }*/
        setContentView(R.layout.activity_main);
        mUnbinder = ButterKnife.bind(this);

        mToolbar.setTitle("首页");
        // 设置ToolBar
        setSupportActionBar(mToolbar);

        // 设置底部导航栏
        initBottomNavigationView();

        // 初始化所有Fragments。
        initFragments();

        // 显示首页fragmetn
        currentFragment = homeFragment;
        mFragmentManager.beginTransaction().add(R.id.fragment_container, homeFragment).show(homeFragment).commit();
    }

    @OnClick(R.id.search_img)
    void startSearchView () {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
        // 去除Activity自身动画
    }

    private void initBottomNavigationView() {
        BottomNavigationItem bottomNavigationItem1 = new BottomNavigationItem ("首页", ContextCompat.getColor(this, R.color.white), R.drawable.ic_home_black_24dp);
        BottomNavigationItem bottomNavigationItem2 = new BottomNavigationItem ("知识", ContextCompat.getColor(this, R.color.white), R.drawable.ic_dashboard_black_24dp);
        BottomNavigationItem bottomNavigationItem3 = new BottomNavigationItem ("套餐", ContextCompat.getColor(this, R.color.white), R.drawable.ic_settings_black_24dp);
        BottomNavigationItem bottomNavigationItem4 = new BottomNavigationItem ("管理", ContextCompat.getColor(this, R.color.white), R.drawable.ic_person_black_24dp);
        navigation.addTab(bottomNavigationItem1);
        navigation.addTab(bottomNavigationItem2);
        navigation.addTab(bottomNavigationItem3);
        navigation.addTab(bottomNavigationItem4);
        navigation.isColoredBackground(false);
        navigation.setItemActiveColorWithoutColoredBackground(getResources().getColor(R.color.green));
        navigation.willNotRecreate(true);
        navigation.disableShadow();
        navigation.setOnBottomNavigationItemClickListener(mOnNavigationItemSelectedListener);

        navigation.selectTab(0);
    }

    public void showFragment(int type) {
        switch (type) {
            case 1:
                navigation.selectTab(1);
                break;
        }
    }

    private void showFragment(Fragment fragment) {
        if (currentFragment != fragment) {
            FragmentTransaction transaction = mFragmentManager.beginTransaction();
            if (mFragments.indexOf(fragment) > mFragments.indexOf(currentFragment)) {
                transaction.setCustomAnimations(R.anim.slide_right_in, R.anim.slide_left_out);
            } else {
                transaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_right_out);
            }
            if (fragment == homeFragment) {
                transaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_right_out);
            }
            if (currentFragment != null) {
                transaction.hide(currentFragment);
            }
            if (!fragment.isAdded()) {
                transaction.add(R.id.fragment_container, fragment).show(fragment).commit();
            } else {
                transaction.show(fragment).commit();
            }
            currentFragment = fragment;
        }
    }

    private void initFragments() {
        mFragmentManager = getSupportFragmentManager();
        homeFragment = HomeFragment.newInstance();
        knowledgeFragment = KnowledgeFragment.newInstance();
        mealFragment = MealFragment.newInstance();
        managerFragment = ManagerFragment.newInstance();
        mFragments.add(homeFragment);
        mFragments.add(knowledgeFragment);
        mFragments.add(mealFragment);
        mFragments.add(managerFragment);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    @Override
    public void onBackPressed() {
        if (currentFragment != homeFragment) {
            navigation.selectTab(0);
        } else {
            super.onBackPressed();
        }
    }

    private OnBottomNavigationItemClickListener mOnNavigationItemSelectedListener = new OnBottomNavigationItemClickListener() {

        @Override
        public void onNavigationItemClick(int index) {
            switch (index) {
                case 0:
                    showFragment(homeFragment);
                    mToolbar.setTitle("首页");
                    break;
                case 1:
                    showFragment(knowledgeFragment);
                    mToolbar.setTitle("知识");
                    break;
                case 2:
                    showFragment(mealFragment);
                    mToolbar.setTitle("套餐");
                    break;
                case 3:
                    showFragment(managerFragment);
                    mToolbar.setTitle("管理");
                    break;
            }
        }
    };
}
