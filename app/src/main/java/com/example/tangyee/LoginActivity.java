package com.example.tangyee;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.tangyee.fragment.LoginFragment;
import com.example.tangyee.fragment.RegisterFragment;
import com.example.tangyee.utils.DensityUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 提供一个使用email 和 密码 登陆的界面
 */
public class LoginActivity extends AppCompatActivity {

    private static final int GOTO_LOGIN = 0;
    private static final int GOTO_REGISTER = 1;

    private Unbinder mUnbinder;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    private List<Fragment> mViewList = new ArrayList<>();
    private MyAdapter mViewPageAdapter;

    @BindView(R.id.login_text_view)
    TextView loginTextView;
    @BindView(R.id.register_text_view)
    TextView registerTextView;

    private List<String> titles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

         mUnbinder = ButterKnife.bind(this);

        loginTextView.setOnClickListener(v -> mViewPager.setCurrentItem(0, true));
        registerTextView.setOnClickListener(v -> mViewPager.setCurrentItem(1, true));

        //Disable clip to padding
        mViewPager.setClipToPadding(false);
        mViewPager.setPadding(DensityUtils.dp2px(this,30), 0, DensityUtils.dp2px(this,30), 0);
        mViewPager.setPageMargin(DensityUtils.dp2px(this,20));
        mViewPager.setPageTransformer(false, new DepthPageTransformer());

        mViewList.add(LoginFragment.newInstance());
        mViewList.add(RegisterFragment.newInstance());

        titles.add("登陆");
        titles.add("注册");

        mViewPageAdapter = new MyAdapter(getSupportFragmentManager(), mViewList, titles);
        mViewPager.setAdapter(mViewPageAdapter);
        mViewPager.setOffscreenPageLimit(2);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){
                    startAnimator(GOTO_LOGIN);
                    return;
                }
                if (position == 1) {
                    startAnimator(GOTO_REGISTER);
                    return;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void startAnimator(int type) {
        if (type == GOTO_REGISTER) {
            float x1 = loginTextView.getLeft();
            float x2 = registerTextView.getLeft();
            float distance = x2 - x1;
            loginTextView.animate().setDuration(200).scaleX(0.5f).scaleY(0.5f).translationX(-distance);
            registerTextView.animate().setDuration(200).scaleX(1f).scaleY(1f).translationX(-distance);
            return;
        }
        if (type == GOTO_LOGIN){
            /*回归原处*/
            loginTextView.animate().setDuration(200).scaleX(1f).scaleY(1f).translationX(0);
            registerTextView.animate().setDuration(200).scaleX(0.5f).scaleY(0.5f).translationX(0);
            return;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

    class MyAdapter extends FragmentPagerAdapter{
        List<Fragment> fragments;
        List<String> list;
        public MyAdapter(FragmentManager fm, List<Fragment> fragments, List<String> list) {
            super(fm);
            this.fragments=fragments;
            this.list=list;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

       @Override
        public CharSequence getPageTitle(int position) {
            return list.get(position);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.7f;
        private static final float MIN_ALPHA = 0.6f;

        @TargetApi(Build.VERSION_CODES.HONEYCOMB)
        @SuppressLint("NewApi")
        public void transformPage(View view, float position) {
            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setScaleY(MIN_SCALE);
                view.setAlpha(MIN_ALPHA);
            } else if (position == 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setScaleY(MIN_SCALE);
                view.setAlpha(MIN_ALPHA);
            } else if (position <= 1) { // (0,1]
                // Fade the page out.

                // Counteract the default slide transition

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE + (1 - MIN_SCALE)* (1 - Math.abs(position));
                float alphaFactor = MIN_ALPHA + (1 - MIN_ALPHA)* (1 - Math.abs(position));
                view.setScaleY(scaleFactor);
                view.setAlpha(alphaFactor);
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setScaleY(MIN_SCALE);
                view.setAlpha(MIN_ALPHA);
            }
        }
    }
}

