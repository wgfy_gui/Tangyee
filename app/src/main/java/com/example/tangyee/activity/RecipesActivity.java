package com.example.tangyee.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.tangyee.R;
import com.example.tangyee.adapter.MyRecyclerViewAdapter;
import com.example.tangyee.bean.Recipe;
import com.example.tangyee.utils.GetRecipes;
import com.example.tangyee.utils.GlideApp;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RecipesActivity extends AppCompatActivity {

    private static final String TAG = "RecipesActivity";

    private Unbinder mUnbinder;

    @BindView(R.id.health_video_back)
    LinearLayout back;
    @BindView(R.id.health_video_title)
    TextView title;

    @BindView(R.id.video_recycler_view)
    RecyclerView mRecyclerView;

    private MyRecyclerViewAdapter<Recipe> mAdapter;
    private List<Recipe> data = new ArrayList<>();
    private View footView;
    private GridLayoutManager layoutManager;

    private int page = 1;
    private int pagesize = 16;

    private boolean isLoading = false;

    private GetRecipes mGetRecipes;

    boolean isClear = false;

    @BindView(R.id.refreshLayout)
    RefreshLayout refreshLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_videos);
        mUnbinder = ButterKnife.bind(this);

        /*
         * 下拉刷新
         * */
        refreshLayout.setRefreshHeader(new MaterialHeader(this));
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            if (mGetRecipes != null) {
                mGetRecipes.cancel(true);
            }
            ProgressBar progressBar = footView.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            TextView textView = footView.findViewById(R.id.loading);
            textView.setText("努力加载中···");
            page = 1;
            isClear = true;
            mGetRecipes = new GetRecipes(page, pagesize, execute);
            mGetRecipes.execute();
        });

        title.setText("菜谱");

        initRecyclerView();
    }

    GetRecipes.OnPostExecute execute;
    private void initRecyclerView() {
        mAdapter = new MyRecyclerViewAdapter<>(data, this, R.layout.recipe_item_list_layout);
        footView = LayoutInflater.from(this).inflate(R.layout.item_foot_layout,null);
        mAdapter.addFooterView(footView);
        mAdapter.setItemViewHelper(new MyRecyclerViewAdapter.ItemViewHelper<Recipe>() {

            @Override
            public void onClick(View view, Recipe recipe) {
                ImageView imageView = view.findViewById(R.id.recipe_image);
                Intent intent = RecipeDetailActivity.getRecipeDetailActivityIntent(RecipesActivity.this, recipe);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(RecipesActivity.this, imageView, "recipe").toBundle());
                } else {
                    startActivity(intent);
                }
            }

            @Override
            public void onBindViewHolder(MyRecyclerViewAdapter.MyHolder holder, int position, Recipe recipe) {
                TextView title = holder.itemView.findViewById(R.id.title);
                title.setText(recipe.getRecipeName());
                TextView heat = holder.itemView.findViewById(R.id.heat);
                heat.setText("浏览  " + recipe.getReadingNum());

                ImageView imageView = holder.itemView.findViewById(R.id.recipe_image);
                GlideApp.with(RecipesActivity.this)
                        .load(recipe.getImageUrl())
                        .error(R.drawable.error)
                        .fitCenter()
                        .into(imageView);

            }
        });
        mRecyclerView.setAdapter(mAdapter);
        layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                if (i == data.size() && i % 2 == 0) {
                    // 占有2列 | 跨度为2
                    return 2;
                }
                return 1;
            }
        });
        mRecyclerView.setLayoutManager(layoutManager);

        execute = items -> {
            if (items == null) {
                if (refreshLayout != null)
                    refreshLayout.finishRefresh(false);
                ProgressBar progressBar = footView.findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);
                TextView textView = footView.findViewById(R.id.loading);
                textView.setText("请求超时···");
            } else if (items.size() == 0) {
                if (refreshLayout != null)
                    refreshLayout.finishRefresh(true);
                ProgressBar progressBar = footView.findViewById(R.id.progressBar);
                progressBar.setVisibility(View.GONE);
                TextView textView = footView.findViewById(R.id.loading);
                textView.setText("没有数据了···");
            } else {
                ++ page;
                if (refreshLayout != null)
                    refreshLayout.finishRefresh(true);
                int count = mAdapter.getItemCount();
                mAdapter.updateDate(items, isClear);

                if (count <= 1 || isClear) {
                    isClear = false;
                    ScaleAnimation sa = new ScaleAnimation(1f, 1f, 1.1f, 1f,
                            Animation.RELATIVE_TO_SELF, 0f,
                            Animation.RELATIVE_TO_SELF, 0f);
                    sa.setDuration(1000);

                    AlphaAnimation aa = new AlphaAnimation(0, 1);
                    aa.setDuration(1000);

                    AnimationSet as = new AnimationSet(true);
                    as.setDuration(1000);
                    as.addAnimation(sa);
                    as.addAnimation(aa);

                    mRecyclerView.startAnimation(as);
                }
            }

            // 设置为false,表示可以再次请求数据了
            isLoading = false;
        };

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition();
                if (lastVisibleItemPosition + 1 == mAdapter.getItemCount() && !isLoading) { // footItem 可见时，说明该加载更多了。
                    Log.d("test", "loading executed");
                    isLoading = true;
                    new GetRecipes(page, pagesize, execute).execute();
                }
            }

            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                try {

                    /*java.lang.IllegalArgumentException: You cannot start a load for a destroyed activity
                    at com.bumptech.glide.manager.RequestManagerRetriever.assertNotDestroyed(RequestManagerRetriever.java:323)*/

                    /*glide优化，提升性能?*/
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        GlideApp.with(RecipesActivity.this).resumeRequests();
                    } else {
                        GlideApp.with(RecipesActivity.this).pauseRequests();
                    }
                } catch (IllegalArgumentException iae) {
                    Log.e(TAG, "onScrollStateChanged: ", iae);
                }
            }
        });

    }

    @OnClick(R.id.health_video_back)
    public void back(View view) {
        onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGetRecipes != null) {
            mGetRecipes.cancel(true);
        }
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }

}
