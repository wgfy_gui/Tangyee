package com.example.tangyee.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tangyee.R;
import com.example.tangyee.bean.Doctor;
import com.example.tangyee.utils.GetDoctors;
import com.example.tangyee.utils.GlideApp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DoctorDetailActivity extends AppCompatActivity {

    private static final String TAG = "DoctorDetailActivity";
    private static final String PARAM = "Doctor";

    private Unbinder mUnbinder;

    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.top_title)
    TextView title;

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.describe)
    TextView describe;
    @BindView(R.id.img)
    ImageView img;

    @BindView(R.id.major)
    TextView major;
    @BindView(R.id.job_title)
    TextView jobTitle;
    @BindView(R.id.detail)
    TextView detail;

    private Doctor mDoctor;
    private GetDoctorDetail mGetDoctorDetail;

    public static Intent getRecipeDetailActivityIntent(Context context, Doctor doctor) {
        Intent intent = new Intent(context, DoctorDetailActivity.class);
        intent.putExtra(PARAM, doctor);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_detail);
        mUnbinder = ButterKnife.bind(this);

        mDoctor = getIntent().getParcelableExtra(PARAM);
        name.setText(mDoctor.getName());
        GlideApp.with(this)
                .load(mDoctor.getImgUrl())
                .error(R.drawable.error)
                .fitCenter()
                .into(img);

        mGetDoctorDetail = new GetDoctorDetail();
        mGetDoctorDetail.execute();

    }

    private class GetDoctorDetail extends AsyncTask<Void, Void, Doctor> {

        @Override
        protected Doctor doInBackground(Void... voids) {
            if (isCancelled()) {
                return null;
            }
            try {
                Document document = Jsoup.connect(mDoctor.getDetailUrl()).get();
                Elements lis = document.select("ul.mui-table-view>li");
                Element li = lis.get(1);
                String major = li.select("span.mui-pull-right").text();
                mDoctor.setMajor(major);

                /*补充医生的毕业院校描述*/
                li = lis.get(0);
                String desc = li.select("span.mui-badge").text();
                mDoctor.setDescribe(desc);

                li = lis.get(2);
                String jobTitle = li.select("span.mui-pull-right").text();
                mDoctor.setJobTitle(jobTitle);

                li = lis.get(3);
                String detail = li.text();
                mDoctor.setDetail(detail);

                Log.d(TAG, "doInBackground: " + mDoctor.toString());

            } catch (SocketTimeoutException ste) {
                Log.e(TAG, "doInBackground: ", ste);
                return null;
            } catch (IOException e) {
                Log.e(TAG, "doInBackground: ", e);
                return null;
            }
            return mDoctor;
        }

        @Override
        protected void onPostExecute(Doctor doctor) {
            if (doctor == null) {
                Toast.makeText(DoctorDetailActivity.this, "请求超时", Toast.LENGTH_SHORT).show();
                return;
            }
            major.setText(doctor.getMajor());
            jobTitle.setText(doctor.getJobTitle());
            detail.setText(doctor.getDetail());

            // 补充医生毕业院校描述
            describe.setText(mDoctor.getDescribe());
        }
    }

    @OnClick(R.id.back)
    public void back() {
        this.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGetDoctorDetail != null) {
            mGetDoctorDetail.cancel(true);
        }
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }
}
