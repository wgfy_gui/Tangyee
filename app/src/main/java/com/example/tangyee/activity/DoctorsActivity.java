package com.example.tangyee.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.tangyee.R;
import com.example.tangyee.bean.Doctor;
import com.example.tangyee.utils.GetDoctors;
import com.example.tangyee.utils.GlideApp;
import com.scwang.smartrefresh.header.MaterialHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class DoctorsActivity extends AppCompatActivity {

    private static final String TAG = "DoctorsActivity";

    private Unbinder mUnbinder;

    @BindView(R.id.back)
    LinearLayout back;
    @BindView(R.id.top_title)
    TextView title;

    @BindView(R.id.doctors_recycler_view)
    RecyclerView mRecyclerView;

    @BindView(R.id.loading_view)
    View loadingView;

    private MyAdapter mAdapter;
    private List<Doctor> data = new ArrayList<>();
    private GridLayoutManager layoutManager;

    private GetDoctors mGetDoctors;

    @BindView(R.id.refreshLayout)
    RefreshLayout refreshLayout;

    GetDoctors.OnPostExecute execute;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors);

        mUnbinder = ButterKnife.bind(this);

        /*
         * 下拉刷新
         * */
        refreshLayout.setRefreshHeader(new MaterialHeader(this));
        refreshLayout.setOnRefreshListener(refreshlayout -> {
            if (mGetDoctors != null) {
                mGetDoctors.cancel(true);
            }
            ProgressBar progressBar = loadingView.findViewById(R.id.progressBar);
            progressBar.setVisibility(View.GONE);
            TextView textView = loadingView.findViewById(R.id.loading);
            textView.setText("努力加载中···");
            mGetDoctors = new GetDoctors(execute);
            mGetDoctors.execute();
        });

        initRecyclerView();

        execute = doctors -> {
            loadingView.setVisibility(View.GONE);
            refreshLayout.finishRefresh(true);
            mAdapter.setDoctorList(doctors);

            ScaleAnimation sa = new ScaleAnimation(1f, 1f, 1.1f, 1f,
                    Animation.RELATIVE_TO_SELF, 0f,
                    Animation.RELATIVE_TO_SELF, 0f);
            sa.setDuration(1000);

            AlphaAnimation aa = new AlphaAnimation(0, 1);
            aa.setDuration(1000);

            AnimationSet as = new AnimationSet(true);
            as.setDuration(1000);
            as.addAnimation(sa);
            as.addAnimation(aa);
            mRecyclerView.startAnimation(as);
        };
        mGetDoctors = new GetDoctors(execute);
        mGetDoctors.execute();
    }

    private void initRecyclerView() {
        mAdapter = new MyAdapter(data);
        mRecyclerView.setAdapter(mAdapter);
        layoutManager = new GridLayoutManager(this,3);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
    }

    @OnClick(R.id.back)
    public void back() {
        this.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGetDoctors != null) {
            mGetDoctors.cancel(true);
        }
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }
    }


    private class MyAdapter extends RecyclerView.Adapter<MyHolder> {

        private List<Doctor> mDoctorList = new ArrayList<>();

        void setDoctorList(List<Doctor> doctorList) {
            mDoctorList = doctorList;
            notifyDataSetChanged();
        }

        MyAdapter(List<Doctor> doctors) {
            mDoctorList = doctors;
        }

        @NonNull
        @Override
        public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            MyHolder holder = new MyHolder(LayoutInflater.from(DoctorsActivity.this).inflate(R.layout.doctor_item_layout, viewGroup,false));
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull MyHolder myHolder, int i) {
            Doctor doctor = mDoctorList.get(i);
            myHolder.name.setText(doctor.getName());
            GlideApp.with(DoctorsActivity.this)
                    .load(doctor.getImgUrl())
                    .error(R.drawable.error)
                    .fitCenter()
                    .into(myHolder.img);
            
            myHolder.itemView.setOnClickListener(v -> {
                Intent intent = DoctorDetailActivity.getRecipeDetailActivityIntent(DoctorsActivity.this, doctor);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(DoctorsActivity.this, myHolder.img, "doctor").toBundle());
                } else {
                    startActivity(intent);
                }
            });
        }

        @Override
        public int getItemCount() {
            if (mDoctorList == null) {
                return 0;
            }
            return mDoctorList.size();
        }

    }

    private class MyHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView img;

        MyHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            img = itemView.findViewById(R.id.img);
        }
    }
}
