package com.example.tangyee.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.Log;

import com.example.tangyee.R;
import com.example.tangyee.utils.DensityUtils;

public class BarChartView extends AppCompatTextView {
    private static final String TAG = "BarChartView";

    private float width;
    private float height;

    private float barHeight;
    private float padding;

    private int barsNum = 3;

    private Paint mBarPaint1;
    private Paint mBarPaint2;
    private Paint mBarPaint3;
    private Paint[] mBarPaints;

    private Paint mTextPaint1;
    private Paint mTextPaint2;
    private Paint mTextPaint3;
    private Paint[] mTextPaints;

    private float textSize;

    private int[] datas = new int[]{0,0,0};
    private int[] tempDatas = new int[]{0,0,0};

    private int step = 0;
    private int unitLen = 0;

    private String[] titles = new String[]{"偏低", "正常", "偏高"};

    public BarChartView(Context context) {
        this(context, null, 0);
        // 不可用super,因为这将会是调用父类构造器,而不调用自己本类的构造器
    }

    public BarChartView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BarChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mBarPaint1 = new Paint();
        mBarPaint1.setColor(getResources().getColor(R.color.low));
        mBarPaint2 = new Paint();
        mBarPaint2.setColor(getResources().getColor(R.color.good));
        mBarPaint3 = new Paint();
        mBarPaint3.setColor(getResources().getColor(R.color.high));
        mBarPaints = new Paint[]{mBarPaint1, mBarPaint2, mBarPaint3};

        textSize = DensityUtils.dp2px(context, 15);
        Log.d(TAG, "BarChartView: " + textSize);

        mTextPaint1 = new Paint();
        mTextPaint1.setColor(Color.BLACK);
        mTextPaint1.setAntiAlias(true);
        mTextPaint1.setTextAlign(Paint.Align.LEFT);
        mTextPaint2 = new Paint();
        mTextPaint2.setColor(Color.BLACK);
        mTextPaint2.setAntiAlias(true);
        mTextPaint2.setTextAlign(Paint.Align.LEFT);
        mTextPaint3 = new Paint();
        mTextPaint3.setColor(Color.BLACK);
        mTextPaint3.setAntiAlias(true);
        mTextPaint3.setTextAlign(Paint.Align.LEFT);

        mTextPaints = new Paint[]{mTextPaint1, mTextPaint2, mTextPaint3};

        padding = DensityUtils.dp2px(context, 15);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (width == 0 || height == 0) {
            return;
        }

        boolean flag = false;
        for (int i = 0; i < barsNum; i ++) {
            float left = 20;
            float top = padding + i * (barHeight+padding);
            float right = tempDatas[i];
            float bottom = padding + i * (barHeight+padding) + barHeight;
            // 画矩形
            canvas.drawRect(
                    left + textSize + left,
                    top,
                    left + textSize + left + right,
                    bottom,
                    mBarPaints[i]);
            drawTextVertical(canvas, titles[i], tempDatas[i], i, left, top + barHeight, right, bottom, mTextPaints[i]);

            if (tempDatas[i] < datas[i]) {
                tempDatas[i] += step;
                flag = true;
            } else {
                tempDatas[i] = datas[i];
            }
        }
        if (flag) {
            invalidate();
        }

    }

    public void setDatas(int[] ds) {
        int maxData = Math.max(Math.max(ds[0], ds[1]), ds[2]);
        double maxW = width - textSize*4 - 50;
        datas = ds;
        if (maxData != 0) {
            unitLen = (int) (maxW / maxData);

            // 每一次重绘添加60分之一的宽度。
            step = (int) (maxW / 60);

            datas[0] = ds[0] * unitLen;
            datas[1] = ds[1] * unitLen;
            datas[2] = ds[2] * unitLen;
        }

        Log.d(TAG, "setDatas: " + barHeight);

        tempDatas[0] = tempDatas[1] = tempDatas[2] = 0;
        invalidate();
    }

    private void drawTextVertical(Canvas canvas, String text, int time, int index, float left, float top,float right, float bottom, Paint paint) {
        // 画文字
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        // 基线到字体上边框的距离。
        float bottomOffset = fontMetrics.bottom;

        String s;
        if (unitLen == 0 || datas[index] == 0) {
            s = "暂无记录";
            Paint p = new Paint();
            p.setColor(Color.GRAY);
            p.setAntiAlias(true);
            p.setTextAlign(Paint.Align.CENTER);
            p.setTextSize(textSize);
            canvas.drawText(s, width/2, bottom - (barHeight - textSize) / 2 - bottomOffset / 2, p);
        } else {
            s = time/unitLen + "次";
            canvas.drawText(s, left + textSize + left + right + left, bottom - (barHeight - textSize) / 2 - bottomOffset / 2, paint);
        }

        top = top - textSize * (text.length()-1) - bottomOffset/2;
        for (int i = 0; i < text.length(); i ++) {
            char t = text.charAt(i);
            canvas.drawText(String.valueOf(t), left, top, paint);
            top += textSize;
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        /*
        * 直接使用getWidth\getHeight不太好，在有些手机上有问题
        * */
        width = measureWidth(widthMeasureSpec);
        height = measureHeight(heightMeasureSpec);

        setMeasuredDimension((int)width, (int)height);

        if (width == 0 || height == 0) {
            return;
        }

        Log.d(TAG, "onMeasure: ");

        barHeight = (height - padding * 4) / barsNum;
        textSize = barHeight/2;
        mTextPaint1.setTextSize(textSize);
        mTextPaint2.setTextSize(textSize);
        mTextPaint3.setTextSize(textSize);

    }

    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = 200;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }
    private int measureHeight(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = 400;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }
}
