package com.example.tangyee.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.TextView;

import com.example.tangyee.R;
import com.lxj.xpopup.core.BottomPopupView;

public class MyPopup extends BottomPopupView implements View.OnClickListener {
    private Context mContext;
    private PopupAdapter mPopupAdapter;

    TextView cancel;
    TextView sure;
    TextView title;
    TextView number;
    TextView number1;
    TextView number2;
    TextView number3;
    TextView number4;
    TextView number5;
    TextView number6;
    TextView number7;
    TextView number8;
    TextView number9;
    TextView number0;
    TextView dot;
    TextView delete;

    public MyPopup(Context context, PopupAdapter popupAdapter){
        this(context);
        this.mPopupAdapter = popupAdapter;
    }

    private MyPopup(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    // 返回自定义弹窗的布局
    @Override
    protected int getImplLayoutId() {
        return R.layout.custom_popup;
    }

    // 执行初始化操作，比如：findView，设置点击，或者任何你弹窗内的业务逻辑
    @Override
    protected void initPopupContent() {
        super.initPopupContent();
        cancel = findViewById(R.id.cancel);
        cancel.setOnClickListener(this);
        sure = findViewById(R.id.sure);
        sure.setOnClickListener(this);
        title = findViewById(R.id.title);
        title.setText(mPopupAdapter.getTitle());
        number = findViewById(R.id.number);
        number.setText(mPopupAdapter.getNumberStr());
        number1 = findViewById(R.id.number1);
        number1.setOnClickListener(this);
        number2 = findViewById(R.id.number2);
        number2.setOnClickListener(this);
        number3 = findViewById(R.id.number3);
        number3.setOnClickListener(this);
        number4 = findViewById(R.id.number4);
        number4.setOnClickListener(this);
        number5 = findViewById(R.id.number5);
        number5.setOnClickListener(this);
        number6 = findViewById(R.id.number6);
        number6.setOnClickListener(this);
        number7 = findViewById(R.id.number7);
        number7.setOnClickListener(this);
        number8 = findViewById(R.id.number8);
        number8.setOnClickListener(this);
        number9 = findViewById(R.id.number9);
        number9.setOnClickListener(this);
        number0 = findViewById(R.id.number0);
        number0.setOnClickListener(this);
        dot = findViewById(R.id.dot);
        dot.setOnClickListener(this);
        delete = findViewById(R.id.delete);
        delete.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String text = number.getText().toString();
        String temp = null;
        switch (v.getId()) {
            case R.id.cancel:
                dismiss();
                break;
            case R.id.sure:
                double d = Double.valueOf(text.length() > 0 ? text : "0");
                dismiss();
                mPopupAdapter.receiveData(d);
                break;
            case R.id.number1:
                temp = (text + "1");
                break;
            case R.id.number2:
                temp = (text + "2");
                break;
            case R.id.number3:
                temp = (text + "3");
                break;
            case R.id.number4:
                temp = (text + "4");
                break;
            case R.id.number5:
                temp = (text + "5");
                break;
            case R.id.number6:
                temp = (text + "6");
                break;
            case R.id.number7:
                temp = (text + "7");
                break;
            case R.id.number8:
                temp = (text + "8");
                break;
            case R.id.number9:
                temp = (text + "9");
                break;
            case R.id.number0:
                temp = (text + "0");
                break;
            case R.id.dot:
                if (!text.contains(".")) {
                    temp = (text + ".");
                }
                break;
            case R.id.delete:
                if (text.length() > 0) {
                    temp = number.getText().subSequence(0, text.length() - 1).toString();
                }
                break;
        }
        if (temp == null) {
            return;
        }
        if (temp.contains(".")) {
            if (temp.indexOf('.') <= 2) {
                number.setText(temp);
            }
        } else {
            if (temp.startsWith("0")) {
                temp = temp.substring(1);
            }
            if (temp.length() <= 2) {
                number.setText(temp);
            }
        }
    }

    public interface PopupAdapter {
        void receiveData(double d);
        String getTitle();
        String getNumberStr();
    }

}
