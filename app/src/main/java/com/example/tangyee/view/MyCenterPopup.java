package com.example.tangyee.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tangyee.R;
import com.lxj.xpopup.animator.PopupAnimator;
import com.lxj.xpopup.core.CenterPopupView;
import com.lxj.xpopup.util.XPopupUtils;

public class MyCenterPopup extends CenterPopupView {
    private Context mContext;
    private PopupHelper mPopupHelper;
    private String[] items;

    private TextView title;

    private RecyclerView mRecyclerView;
    private ItemAdapter mItemAdapter;
    private LinearLayoutManager mLayoutManager;
    private int offset;

    public MyCenterPopup(Context context, String[] items, PopupHelper popupHelper){
        this(context);
        this.mPopupHelper = popupHelper;
        this.items = items;
    }

    public MyCenterPopup(Context context, String[] items, PopupHelper popupHelper, int offset){
        this(context);
        this.mPopupHelper = popupHelper;
        this.items = items;
        this.offset = offset;
    }

    private MyCenterPopup(@NonNull Context context) {
        super(context);
        mContext = context;
    }

    // 返回自定义弹窗的布局
    @Override
    protected int getImplLayoutId() {
        return R.layout.custom_center_popup_view;
    }

    // 执行初始化操作，比如：findView，设置点击，或者任何你弹窗内的业务逻辑
    @Override
    protected void initPopupContent() {
        super.initPopupContent();
        title = findViewById(R.id.tv_title);
        title.setText(mPopupHelper.getTitle());

        mRecyclerView = findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (offset != 0) {
            mLayoutManager.scrollToPosition(offset);
        }
        mItemAdapter = new ItemAdapter(items);
        mRecyclerView.setAdapter(mItemAdapter);
    }

    // 设置最大宽度
    @Override
    protected int getMaxWidth() {
        return (int) (XPopupUtils.getWindowWidth(getContext()) * 0.76f);
    }

    // 设置最大高度
    @Override
    protected int getMaxHeight() {
        return (int) (XPopupUtils.getWindowHeight(getContext()) * 0.76f);
    }

    // 设置自定义动画
    @Override
    protected PopupAnimator getPopupAnimator() {
        return super.getPopupAnimator();
    }

    public interface PopupHelper {
        String getTitle();
        void onSelect(View view, int position);
    }

    private class ItemHolder extends RecyclerView.ViewHolder {

        public TextView mTextView;

        public ItemHolder(@NonNull View itemView) {
            super(itemView);

            mTextView = (TextView) itemView;
        }
    }

    private class ItemAdapter extends RecyclerView.Adapter<ItemHolder> {

        private String[] items;

        ItemAdapter(String[] items) {
            this.items = items;
        }

        @NonNull
        @Override
        public ItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            View view = layoutInflater.inflate(R.layout.my_simple_list_item, viewGroup, false);
            return new ItemHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ItemHolder itemHolder, int i) {
            itemHolder.mTextView.setText(items[i]);
            itemHolder.mTextView.setOnClickListener(v -> {
                mPopupHelper.onSelect(itemHolder.mTextView, i);
                dismiss();
            });
        }

        @Override
        public int getItemCount() {
            return this.items.length;
        }
    }

}
