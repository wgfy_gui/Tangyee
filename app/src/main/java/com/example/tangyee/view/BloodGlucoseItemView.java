package com.example.tangyee.view;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tangyee.R;
import com.example.tangyee.bean.BloodGlucoseRecord;

import java.util.Calendar;
import java.util.Date;

public class BloodGlucoseItemView extends LinearLayout {

    private static final String TAG = "BloodGlucoseItemView";

    private TextView dateView;
    private TextView textView1;
    private TextView textView2;
    private TextView textView3;
    private TextView textView4;
    private TextView textView5;
    private TextView textView6;
    private TextView textView7;
    private TextView textView8;

    private BloodGlucoseRecord mBloodGlucoseRecord;
    private OnItemClickedListener<BloodGlucoseRecord> mOnItemClickedListener;

    public BloodGlucoseItemView(Context context) {
        super(context);
        init(context);
    }

    public BloodGlucoseItemView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.blood_glucose_one_row_layout, this);

        dateView = findViewById(R.id.date);
        Log.d(TAG, "BloodGlucoseItemView: " + dateView);

        textView1 = findViewById(R.id.text1);
        textView2 = findViewById(R.id.text2);
        textView3 = findViewById(R.id.text3);
        textView4 = findViewById(R.id.text4);
        textView5 = findViewById(R.id.text5);
        textView6 = findViewById(R.id.text6);
        textView7 = findViewById(R.id.text7);
        textView8 = findViewById(R.id.text8);


        dateView.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(dateView, 0, mBloodGlucoseRecord);
            }
        });

        textView1.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView1, 1, mBloodGlucoseRecord);
            }
        });
        textView2.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView2, 2, mBloodGlucoseRecord);
            }
        });
        textView3.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView3, 3, mBloodGlucoseRecord);
            }
        });
        textView4.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView4, 4, mBloodGlucoseRecord);
            }
        });
        textView5.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView5, 5, mBloodGlucoseRecord);
            }
        });
        textView6.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView6, 6, mBloodGlucoseRecord);
            }
        });
        textView7.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView7, 7, mBloodGlucoseRecord);
            }
        });
        textView8.setOnClickListener(v -> {
            if (mOnItemClickedListener != null) {
                mOnItemClickedListener.onItemClicked(textView8, 8, mBloodGlucoseRecord);
            }
        });
    }

    public BloodGlucoseItemView(Context context,  @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setBloodGlucoseRecord(BloodGlucoseRecord bloodGlucoseRecord) {
        this.mBloodGlucoseRecord = bloodGlucoseRecord;

        Date date = bloodGlucoseRecord.getRecordDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String str = calendar.get(Calendar.DATE) + " " + (calendar.get(Calendar.MONTH) + 1) + "月";
        SpannableString spannableString = new SpannableString(str);
        int t = str.indexOf(' ');
        int l = str.length();
        RelativeSizeSpan sizeSpan2 = new RelativeSizeSpan(0.65f);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#000000"));
        spannableString.setSpan(colorSpan, 0, t, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        spannableString.setSpan(sizeSpan2, t, l, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        dateView.setText(spannableString);

        setViewData(textView1, bloodGlucoseRecord.getDataBySign(1), 1);
        setViewData(textView2, bloodGlucoseRecord.getDataBySign(2), 2);
        setViewData(textView3, bloodGlucoseRecord.getDataBySign(3), 3);
        setViewData(textView4, bloodGlucoseRecord.getDataBySign(4), 4);
        setViewData(textView5, bloodGlucoseRecord.getDataBySign(5), 5);
        setViewData(textView6, bloodGlucoseRecord.getDataBySign(6), 6);
        setViewData(textView7, bloodGlucoseRecord.getDataBySign(7), 7);
        setViewData(textView8, bloodGlucoseRecord.getDataBySign(8), 8);
    }


    private void setViewData(TextView textView, double data, int sign) {
        if (data == 0) {
            textView.setText("+");
            textView.setBackgroundResource(R.drawable.cell_text_view);
            return;
        }
        if (data < 4.4) {
            textView.setBackgroundResource(R.drawable.cell_text_view_low);
        } else if (data > 10.0) {
            textView.setBackgroundResource(R.drawable.cell_text_view_high);
        } else {
            textView.setBackgroundResource(R.drawable.cell_text_view_good);
        }

        textView.setText(String.valueOf(data));
    }

    public void setOnItemClickedListener(OnItemClickedListener<BloodGlucoseRecord> itemClickedListener) {
        this.mOnItemClickedListener = itemClickedListener;
    }

}
