package com.example.tangyee.view;

import android.view.View;

public interface OnItemClickedListener<T> {
    void onItemClicked(View view, int index, T item);
}
